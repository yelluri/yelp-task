from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import SGD, Adam, RMSprop
from keras.utils import np_utils
import numpy as np

class ANN():
    def __init__(self):
        """Constructor"""

        self.nb_epoch = 200

    def trainPredict(self,trainX,trainY,testX,testY):

        model = Sequential()
        model.add(Dense(25, input_dim=trainX.shape[1]))
        model.add(Activation('linear'))
        model.add(Dropout(0.5))
        model.add(Dense(8))
        model.add(Activation('relu'))
        model.add(Dense(1))
        model.add(Activation('linear'))

        model.compile(optimizer='rmsprop',
              loss='mse')

        model.fit(trainX,trainY,validation_split=0.1,verbose=1)

        predictedY = model.predict(testX)
        #print predictedY
        return predictedY