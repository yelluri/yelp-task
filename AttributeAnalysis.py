#from Globals import *
import numpy as np
import matplotlib.pyplot as plt
import json
import pandas as pd
import seaborn as sns
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext, SparkSession
from pyspark.sql.functions import array_contains

from pyspark.sql.functions import col, udf
from pyspark.sql.types import IntegerType, StringType

class AttributeAnalysis():
    """For each attribute plot the histogram of star ratings to understand its role in ratings
    """

    def __init__(self):
        self.task = "Ratings vs Attributes"
        self.Analyse()

    def Analyse(self):

        """Read the data and filter only for Restaurants
        """

        conf = SparkConf().setMaster("local").setAppName("My App")
        sc = SparkContext(conf = conf)

        sqlContext = SQLContext(sc)
        spark = SparkSession(sc)

        business = spark.read.json('dataset/yelp_academic_dataset_business.json')

        #Filter only records for restaurants
        restaurants = business.filter(array_contains(business.categories,'Restaurants'))

        self.TrueFalseQuery(restaurants)
        self.SpecialQueries(restaurants)
        self.JointQuery(restaurants,['WiFi: free','NoiseLevel: quiet','RestaurantsGoodForGroups: True'],'Good case')
        self.JointQuery(restaurants,['WiFi: no','NoiseLevel: loud','RestaurantsGoodForGroups: False'],'Worst case')

        return None

    def TrueFalseQuery(self,restaurants):
        """Creates True and False Query for attrs. for better comparison"""

        values = [': True',': False']
        attributes = ['BusinessAcceptsCreditCards','RestaurantsDelivery','RestaurantsGoodForGroups','RestaurantsReservations','RestaurantsTableService','RestaurantsTakeOut','GoodForKids','OutdoorSeating','ByAppointmentOnly','HasTV']
        for attr in attributes:
            for val in values:
                query = attr+val
                self.FilterQuery(restaurants,query)
        return None

    def SpecialQueries(self,restaurants):
        """For special Values queries"""

        queries = ['WiFi: no','WiFi: free','NoiseLevel: loud','NoiseLevel: average','NoiseLevel: quiet']
        for query in queries:
            self.FilterQuery(restaurants,query)
        return None


    def FilterQuery(self,restaurants,query):
        """Filter using the given attr."""

        temp = restaurants.filter(array_contains(restaurants.attributes,query)).select('stars').toPandas().as_matrix().flatten()

        title = query+str(' avg:')+str(round(temp.mean(),1))+str(',Tot:')+str(len(temp))
        self.PlotHistogram(temp,title,query)

        return None

    def JointQuery(self,temp,queries,filename):
        """To view histograms for several attr."""

        #To apply each query cumulatively
        for i in queries:
            temp = temp.filter(array_contains(temp.attributes,i))
        temp = temp.select('stars').toPandas().as_matrix().flatten()


        title = filename+str(' avg:')+str(round(temp.mean(),1))+str(',Tot:')+str(len(temp))
        self.PlotHistogram(temp,title,filename)

        return None


    def PlotHistogram(self,x,title,filename,xaxis='star_rating',yaxis='normed_freq',bins=6):
        """Plots a histogram"""


        #plt.hist(x,bins=bins,normed=1,facecolor='blue')
        ax = sns.distplot(x,bins=bins,norm_hist=True)
        plt.title(title)
        plt.xlabel(xaxis)
        plt.ylabel(yaxis)
        plt.savefig(str('Histograms/')+filename+str('.jpg'))
        plt.clf()

        return None




if __name__ == "__main__":

    Obj = AttributeAnalysis()
    Obj.Analyse()