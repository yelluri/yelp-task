

#Base Image with python, spark modules included
FROM jupyter/pyspark-notebook:latest



USER root

RUN apt-get -y update



#Install certain python packages
RUN pip install --upgrade pip && pip install --upgrade matplotlib && pip install keras && pip install tensorflow


#Copy python scripts and json files to the container.
COPY ./*.py $HOME/work/

COPY ./*.json $HOME/work/dataset/


#No need to run while building
#RUN python $HOME/work/FansAnalysis.py
#RUN python $HOME/work/AttributeAnalysis.py
#RUN python $HOME/work/ReviewAnalysis.py



USER $NB_USER
