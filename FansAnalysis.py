#from Globals import *
import json
import matplotlib.pyplot as plt
import matplotlib
import FinalModel
matplotlib.style.use('ggplot')
import pandas as pd
import seaborn as sns
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext, SparkSession

from pyspark.sql.functions import udf, col
from pyspark.sql.types import IntegerType, StringType

class FansAnalysis():
    """To predict the no. of fans for a user based on his other attributes like friends counts,
     no. and types of compliments recieved etc.
    """

    def __init__(self):
        self.task = "Preprocess and train a model"
        data = self.preprocess()

        ModelObj = FinalModel.FinalModel()
        ModelObj.readDataset(data)
        ModelObj.LinearRegression()
        ModelObj.RandomForest()
        ModelObj.MLP()


    def preprocess(self):
        """Preprocess the data using Apache spark and return a pandas DataFrame"""
        conf = SparkConf().setMaster("local").setAppName("My App")
        sc = SparkContext(conf = conf)


        sqlContext = SQLContext(sc)
        spark = SparkSession(sc)


        users = spark.read.json('dataset/yelp_academic_dataset_user.json')
        reviews = spark.read.json('dataset/yelp_academic_dataset_review.json')

        #Convert array of friends and elite years as total count of friends and elite years
        count = udf(lambda x: len(x),IntegerType())
        users = users.withColumn('friends',count(col('friends')))
        users = users.withColumn('elite',count(col('elite')))

        #Convert yelping since date as num of days till recent date.
        curr_date = pd.to_datetime('2017-02-01')
        days_count = udf(lambda x: (curr_date - pd.to_datetime(x)).days,IntegerType())
        users = users.withColumn('yelping_since',days_count(col('yelping_since')))


        #Compute avg values of useful, funny and cool of reviews for each user
        reviews.registerTempTable('reviews')
        reviews = spark.sql("SELECT AVG(useful) as useful,AVG(funny) as funny,AVG(cool) as cool,AVG(stars) as stars, user_id FROM reviews GROUP BY user_id")
        reviews.registerTempTable('reviews')


        #Join the two tables and convert to pandas for working on prediction
        users.registerTempTable('users')
        users = spark.sql("SELECT users.review_count,users.yelping_since,users.friends,reviews.useful as avguseful,reviews.funny as avgfunny,reviews.cool as avgcool,users.useful,users.funny,users.cool,users.elite,users.average_stars,users.compliment_hot as hot,users.compliment_more as more,users.compliment_profile as profile,users.compliment_cute as cute,users.compliment_list as list,users.compliment_note as note,users.compliment_plain as plain,users.compliment_cool as cool,users.compliment_funny as funny,users.compliment_writer as writer,users.compliment_photos as photos,users.fans FROM reviews, users WHERE users.fans > 0 AND users.user_id = reviews.user_id").toPandas()
        users.to_csv("dataset/data1.csv",index=False)

        return users

    def dataExploration(self):
        """Analyse the data like correlations"""

        self.dataSet = pd.read_csv('dataset/data1.csv')
        CorrMatrix = self.dataSet.corr(method='spearman')
        ax = sns.heatmap(CorrMatrix)
        plt.show()

        return None

if __name__ == "__main__":

    Obj = FansAnalysis()

