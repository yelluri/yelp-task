import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler,StandardScaler,RobustScaler
from sklearn import linear_model,neighbors
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error,r2_score
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestRegressor,AdaBoostRegressor
#from ESN import classicESN, classicTuner
from sklearn.svm import NuSVR
import matplotlib.pyplot as plt
import matplotlib
matplotlib.style.use('ggplot')
#from Globals import *
import ANN
import seaborn as sns
from sklearn.model_selection import cross_val_score
from sklearn.feature_selection import RFE
class FinalModel():
    """Responsible to scale and fit a mathematical model
    """

    def __init__(self):
        self.task = "Responsible to fit a mathematical model"

    def Scatterplots(self):
        """Generates scatter plots for each input feature vs output feature"""

        for feature in self.inputFeatures:
            #ax = self.dataSet.plot(kind='scatter',x=feature,y=self.outputFeatures[0])
            print 'Plotting for feature : '+str(feature)
            ax = sns.regplot(x=self.dataSet[feature],y=self.dataSet[self.outputFeatures[0]])
            plt.savefig('Scatter Plots/'+str(feature)+'.jpg')
            plt.clf()
        return None

    def readDataset(self,data):
        """Reads the dataset"""

        self.dataSet = data
        #self.dataSet = pd.read_csv("dataset/data1.csv")
        self.features = self.dataSet.columns.values.tolist()
        self.inputFeatures,self.outputFeatures = self.features[:-1],self.features[-1:]

        self.Scatterplots()

        self.dataSet = self.dataSet.as_matrix()

        self.dataSet = self.shuffleData ( self.dataSet )                    #Shuffle the data
        self.splitTrainTest()                                               #Split as test and train sets
        self.ScaleData()                                                    #Decorrelate and scale the data
        #self.DimReduction(Dim=5)
        self.dataForCV()

        return None

    def dataForCV(self):
        """combines test and train for cross validation"""

        trainX, trainY, testX, testY = self.splitInputOutput()
        self.fullX = np.vstack((trainX,testX))
        self.fullY = np.vstack((trainY,testY))

        return None


    def DimReduction(self,Dim = 4):
        """Perform Dimensionality reduction using PCA"""

        trainX, trainY, testX, testY = self.splitInputOutput()

        Obj = PCA(n_components=Dim)
        trainX = Obj.fit_transform(trainX)
        testX = Obj.transform(testX)
        self.mergeInputOutput(trainX, trainY, testX, testY)

        return None

    def mergeInputOutput(self,trainX, trainY, testX, testY):
        """Merges the input and output features where output feature is always at the end"""

        self.train = np.hstack((trainX,trainY))
        self.test = np.hstack((testX,testY))

        return None

    def splitTrainTest(self, trainRatio = 0.999):
        """Splits the dataset as train and test set based on the ratio specified
        trainRatio : specify the ratio of points to remain in training set"""

        splitIndex = int(self.dataSet.shape[0] * trainRatio)
        self.train, self.test = self.dataSet[:splitIndex],self.dataSet[splitIndex:]

        return None

    def shuffleData(self,Input):
        """Shuffles the dataset"""

        shuffler = np.arange(Input.shape[0])
        np.random.shuffle(shuffler)

        return Input[shuffler]

    def ScaleData(self,Type = "Robust"):
        """Scales the data
        Type = 'unitVar' or 'MinMax'"""

        if(Type == 'MinMax'):
            Scaler = MinMaxScaler(feature_range=(-1.0,1.0))
        elif(Type == 'unitVar'):
            Scaler = StandardScaler()
        elif(Type == 'Robust'):
            Scaler = RobustScaler()

        self.train = Scaler.fit_transform(self.train)
        self.test = Scaler.transform(self.test)

        #For CV
        self.dataSet = Scaler.fit_transform(self.dataSet)



        return None

    def LinearRegression(self):
        """Use Linear Regression to learn the underlying model"""

        trainX, trainY, testX, testY = self.splitInputOutput()

        print "\n\nUsing Linear Regressor"

        LR = linear_model.LinearRegression()
        LR.fit(trainX,trainY)
        predictedY = LR.predict(testX)

        print "Learned Coeffcients \t", LR.coef_
        rmse = np.sqrt(mean_squared_error(testY, predictedY))
        r2 = r2_score(testY, predictedY)
        print "RMSE: \t",rmse
        print "R2 Score: \t",r2

        #Perform 10-Fold CV
        scores = cross_val_score(LR, self.fullX, self.fullY, cv=10)
        print("Accuracy(Linear Reg.): %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

        #plot true and predicted
        self.plotPrediction("Linear Regressor",testY,predictedY,rmse,r2)

        return None


    def splitInputOutput(self):
        """Splits the dataset as input and output matrices
        Assuming the output or target feature is the last column"""

        trainX, trainY = self.train[:,:(self.train.shape[1]-1)],self.train[:,(self.train.shape[1]-1):]
        testX, testY = self.test[:,:(self.test.shape[1]-1)],self.test[:,(self.test.shape[1]-1):]

        return trainX, trainY, testX, testY

    def plotPrediction(self, ModelName, true, predicted,rmse,r2):
        """Plots the values of true and predicted price"""

        df = pd.DataFrame(data=np.hstack((true, predicted)) , columns= ['true','predicted'])

        #Sort according to true price
        df = df.sort_values(['true'])

        #reset index after sort
        df = df.reset_index(drop=True)

        f = plt.figure()
        df.plot()

        #Add the name and score of the regressor in the title
        plt.title(ModelName + (" (RMSE : %0.2f , R2 : %0.2f)" % (rmse,r2)))
        plt.savefig("Error plots/"+str(ModelName)+".jpg")

        return None




    def RandomForest(self):
        """Use RandomForest Regressor"""

        trainX, trainY, testX, testY = self.splitInputOutput()

        print "\n\nUsing Random Forest..."
        RfObj = RandomForestRegressor()
        paramater_grid = dict(n_estimators = range(100,102))

        #For tuning the no. of tress parameter
        grid = GridSearchCV(RfObj,paramater_grid,cv=3,scoring='r2',n_jobs=-1,verbose=0)
        grid.fit(trainX,trainY)
        predictedY = grid.predict(testX).reshape(testY.shape)

        print "Choosen Parameters: \t",grid.best_params_

        #Perform 10-fold CV
        scores = cross_val_score(RandomForestRegressor(n_estimators=grid.best_params_['n_estimators']), self.fullX, self.fullY, cv=5)
        print("Accuracy (RF): %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))


        rmse = np.sqrt(mean_squared_error(testY, predictedY))
        r2 = r2_score(testY, predictedY)
        print "RMSE: \t",rmse
        print "R2 Score: \t",r2

        #plot true and predicted
        self.plotPrediction("Random Forest (trees: "+str(grid.best_params_['n_estimators'])+")",testY,predictedY,rmse,r2)

        return None


    def MLP(self):
        """use MLP from sklearn"""


        trainX, trainY, testX, testY = self.splitInputOutput()

        print "\n\nUsing MLP"
        MLPObj = ANN.ANN()
        predictedY = MLPObj.trainPredict(trainX, trainY, testX, testY).reshape(testY.shape)

        rmse = np.sqrt(mean_squared_error(testY, predictedY))
        r2 = r2_score(testY, predictedY)
        print "RMSE: \t",rmse
        print "R2 Score: \t",r2

        #plot true and predicted
        self.plotPrediction("MLP",testY,predictedY,rmse,r2)

        return None






