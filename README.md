This Repository has the code for data analysis on Yelp dataset : (https://www.yelp.com/dataset_challenge/dataset)

Questions we have tried to answer are:

	1.Predicting user's Fan count.
		- using features like no. of friends, likes, reviews, etc.
	2.Analysis on effect of attributes on business' ratings.
		- using attributes like WiFi, Noise Level, etc.
	3.Predicting the star rating of a review text.
		- performing NLP and text classifcation and predicting the star rating.


For running this module in a DOCKER container, 
	
	- Having Docker installed, pull the image using : docker pull yelluri/docker-yelpfinal

	- run the image using : docker logs -f $(docker run -d -p 8888:8888 yelluri/docker-yelpfinal)

	- Go to : http://localhost:8888/tree?token=<token-id> . Eg: http://localhost:8888/tree?token=61ee692cf760b20692724740bab967ac75fc0f029f522270 (this token id would should up in console when we use docer run).
	
	- Open a new jupyter Notebook with (Python 3) kernel, enter 'run <filename>.py' and execute the cell. Eg filenames : FansAnalysis, ReviewAnalysis & AttributeAnalysis.
		

For executing the code :

	- Please place the extracted json files inside dataset folder.

	- Modules Required:

		Python 2.7, Apache Spark(Pyspark), Sklearn, Pandas, keras and seaborn.

	- Run FansAnalysis.py, AttributeAnalysis.py and ReviewAnalysis.py for solutions 1,2 and 3 questions.
