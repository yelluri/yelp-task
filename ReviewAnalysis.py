#from Globals import *
import json
import matplotlib.pyplot as plt
import matplotlib
matplotlib.style.use('ggplot')
import pandas as pd
import string
import numpy as np
import seaborn as sns

import sklearn.datasets
import sklearn.feature_extraction.text
import sklearn.naive_bayes
import sklearn.model_selection
import sklearn.svm
import sklearn.ensemble
from sklearn.model_selection import cross_val_score

from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext, SparkSession

from pyspark.sql.functions import array_contains
from pyspark.sql.functions import udf, col
from pyspark.sql.types import IntegerType, StringType

class ReviewAnalysis():
    """With the Review text, we try to predict the star ratings
    """

    def __init__(self):
        self.task = "Preprocess and train a model to do text classification"
        data = self.preprocess()
        self.Classify(data)

    def preprocess(self):
        """Read dataset, filter and remove stop words and punctuations in them"""

        conf = SparkConf().setMaster("local").setAppName("My App")
        sc = SparkContext(conf = conf)

        sqlContext = SQLContext(sc)
        spark = SparkSession(sc)

        review = spark.read.json('dataset/yelp_academic_dataset_review.json')
        business = spark.read.json('dataset/yelp_academic_dataset_business.json')

        #For relatedness lets consider the reviews of only restaurents in US alone
        US_States=['SC', 'NC', 'AZ', 'MN', 'AK', 'FL', 'TX', 'PA', 'NM', 'AL', 'IL', 'CA', 'WI', 'NV', 'TAM']
        Indicator = udf(lambda x: 1 if x in US_States else 0)
        business = business.withColumn('state',Indicator(col('state'))).filter(array_contains(business.categories,'Restaurants')).filter(col('state') == 1).select('business_id')
        review = review.join(business,'business_id').select('text','stars')
        #deleting for saving memory
        del(business)

        #convert all words to lowercase
        cleanText = udf(lambda x: ' '.join(x.lower().replace('\n',' ').split()))
        review = review.withColumn('text',cleanText(col('text')))
        #Remove special Characters
        blank_string = ' '*len(string.punctuation)
        removePunctuations = udf(lambda x: str(x.encode('utf8')).translate(string.maketrans(string.punctuation,blank_string)),StringType())
        review = review.withColumn('text',removePunctuations(col('text')))

        #word count of each review
        count = udf(lambda x: len(x.split()),IntegerType())
        review = review.withColumn('wordCount',count(col('text')))

        #For ease let us consider small review texts, which are usually straightforward
        review = review.filter(col('wordCount')<50).filter(col('wordCount')>7).select('text','stars')

        print review.first()
        print review.count()
        review =review.toPandas()
        #review.to_csv("dataset/review.csv",index=False,encoding='utf-8')
        return None

    def Classify(self,data):
        """Learn a model for predicting the rating of a review"""

        self.dataSet =data#pd.read_csv("dataset/review.csv",encoding='utf8',nrows=200000)

        #split the input and target variables as reviews and ratings resp.
        reviews = self.dataSet.text.tolist()
        ratings = self.dataSet.stars.tolist()

        #Compute bag of words matrix for the review text
        BoW = self.BagOfWords(reviews)

        #Compute TfIdf matrix
        TfIdf = self.TfIdfTransform(BoW)


        Classifier = sklearn.svm.LinearSVC(C=5)
        #Classifier = sklearn.ensemble.RandomForestClassifier(250,verbose=1,n_jobs=-1,max_depth=200)
        #Classifier = sklearn.naive_bayes.MultinomialNB()
        self.RunClassifier(TfIdf,ratings,Classifier)

        return None

    def BagOfWords(self,reviews):
        """Convert the given set of reviews as a Bag of words repr. (a sparse matrix)"""

        Obj = sklearn.feature_extraction.text.CountVectorizer()
        return Obj.fit_transform(reviews)

    def TfIdfTransform(self,Bow):
        """Compute the TfIdf matrix which is weighted by tf and idf"""

        Obj = sklearn.feature_extraction.text.TfidfTransformer(use_idf=True).fit(Bow)
        return Obj.transform(Bow)

    def RunClassifier(self,Input,Output,Classifier,test_ratio=0.2):
        """Run the classifier and display the results"""

        #Split Data
        trainX, testX, trainY, testY = sklearn.model_selection.train_test_split(Input, Output, test_size=test_ratio,stratify=Output)

        #train and predict the ratings
        Classifier.fit(trainX, trainY)
        predictedY = Classifier.predict(testX)

        #print the classification score
        print sklearn.metrics.classification_report(testY,predictedY)
        cnfMatrix = sklearn.metrics.confusion_matrix(testY,predictedY)
        print cnfMatrix
        self.HeatMapOfConfusionMatrix(1.*cnfMatrix)

        #Perform Cross validation for a better score
        scores = cross_val_score(Classifier, Input, Output, cv=5)
        print("Accuracy (RF): %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

        return None
    def HeatMapOfConfusionMatrix(self,matrix):
        """Plot a heat map of the confusion matrix"""

        #row normed matrix
        matrix = (matrix.T/matrix.sum(axis=1)).T
        print matrix
        labels = [1,2,3,4,5]
        ax = sns.heatmap(matrix,xticklabels=labels,yticklabels=labels,cmap='YlGnBu')
        plt.show()
        plt.savefig('temp.jpg')
        return None

if __name__ == "__main__":

    Obj = ReviewAnalysis()
    data = Obj.preprocess()
    Obj.Classify(data)